package com.Wipro.departmentservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Wipro.departmentservice.entity.Department;
import com.Wipro.departmentservice.repository.DepartmentRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DepartmentService {
	
	@Autowired
	private DepartmentRepository departmentrepository;

	public Department saveDepartment(Department department) {
		log.info("inside saveDepartment of DepartmentController");
		return departmentrepository.save(department);
	}

	public Department findDepartmentById(Long departmentId) {
		log.info("inside saveDepartment of DepartmentController");
		return departmentrepository.findByDepartmentId(departmentId);
	}
}
