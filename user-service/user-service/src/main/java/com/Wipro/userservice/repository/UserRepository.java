package com.Wipro.userservice.repository;

import com.Wipro.userservice.entity.User;

public interface UserRepository {

	User findByUserId(Long userId);

}
